import userRepository from '../../data/repositories/userRepository';
import { encryptSync } from '../../helpers/cryptoHelper';

export const getUserById = async userId => {
  const { id, username, status, email, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, status, email, imageId, image };
};

export const updateUserById = async (userId, userData) => {
  const {
    id,
    username,
    status,
    email,
    imageId,
    image
  } = await userRepository.updateUserById(userId, userData);
  return { id, username, status, email, imageId, image };
};

export const updateUserPasswordById = async (userId, password) => {
  const encryptedPassword = encryptSync(password);
  const { id, username, status, email, imageId, image } = await updateUserById(userId, { password: encryptedPassword });
  return { id, username, status, email, imageId, image };
};
