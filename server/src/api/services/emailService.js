import nodemailer from 'nodemailer';
import * as config from '../../config/emailConfig';

const transporter = nodemailer.createTransport({
  service: config.service,
  auth: {
    user: config.user,
    pass: config.password
  }
});

export const sendResetPasswordEmail = async (email, userId, resetPasswordToken) => {
  const mailOptions = {
    from: config.user,
    to: email,
    subject: 'Reset password',
    text: `Your reset password link: http://localhost:3000/reset-password/${userId}/${resetPasswordToken}`
  };

  const response = await transporter.sendMail(mailOptions);
  return response;
};

export const sendNotificationEmail = async (sender, recepient, postLink) => {
  const mailOptions = {
    from: config.user,
    to: recepient.email,
    subject: 'Thread notification',
    text: `Hello
your post was liked by ${sender.username}
Post: ${postLink}`
  };

  const response = await transporter.sendMail(mailOptions);
  return response;
};

export const sendSharePostLink = async (senderUsername, recepientEmail, postLink) => {
  const mailOptions = {
    from: config.user,
    to: recepientEmail,
    subject: 'Thread notification',
    text: `Hello
${senderUsername} shared a post with you.
Have a look: ${postLink}`
  };

  const response = await transporter.sendMail(mailOptions);
  return response;
};
