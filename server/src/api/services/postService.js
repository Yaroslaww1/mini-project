/* eslint-disable no-unused-vars */
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = async postId => postRepository.getPostById(postId);

export const updatePostById = async (postId, body) => postRepository.updatePostById(postId, body);

export const deletePostById = async postId => postRepository.deletePostById(postId);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  const post = await postRepository.getPostById(postId);

  return {
    post,
    result
  };
};
