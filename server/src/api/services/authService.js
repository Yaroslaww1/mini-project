import { createToken } from '../../helpers/tokenHelper';
import { encrypt, getRandomStringToken } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import resetPasswordRepository from '../../data/repositories/resetPasswordRepository';
import * as emailService from './emailService';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    status: 'No status set',
    password: await encrypt(password)
  });
  return login(newUser);
};

export const resetPasswordByEmail = async email => {
  const user = await userRepository.getByEmail(email);
  if (!user) {
    return { status: 'unsuccess', error: `User with email: ${email} does not exist` };
  }

  try {
    const token = getRandomStringToken().replace(/\//g, '_').replace(/\+/g, '-').replace(/\./g, '-');
    const hash = (await encrypt(token)).replace(/\//g, '_').replace(/\+/g, '-').replace(/\./g, '-');
    const resetPasswordToken = await resetPasswordRepository.createOrUpdateResetPassword(user.id, hash);
    emailService.sendResetPasswordEmail(email, user.id, resetPasswordToken.resetPasswordToken);
    return { status: 'success' };
  } catch (err) {
    return err;
  }
};

export const getResetPasswordByUserId = async userId => {
  const resetPassword = await resetPasswordRepository.getResetPasswordByUserId(userId);
  return resetPassword.resetPasswordToken;
};
