import * as authService from '../services/authService';

const validate = async (req, res, next) => {
  try {
    const { userId, token } = req.params;
    const resetPasswordToken = await authService.getResetPasswordByUserId(userId);
    console.log(resetPasswordToken, token);
    if (resetPasswordToken !== token) {
      throw new Error('token is not valid');
    }
  } catch (err) {
    next(err);
  } finally {
    next();
  }
};

export default validate;
