import * as commentService from '../services/commentService';

export const canEditComment = async (req, res, next) => {
  try {
    const { user: { id } } = req;
    const post = await commentService.getCommentById(req.params.id);
    if (id !== post.user.id) {
      throw new Error(`update comment with id ${req.params.id} is not allowed for user with id ${id}`);
    }
  } catch (err) {
    next(err);
  } finally {
    next();
  }
};
