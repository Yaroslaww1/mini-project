import * as postService from '../services/postService';

export const canEditPost = async (req, res, next) => {
  try {
    const { user: { id } } = req;
    const post = await postService.getPostById(req.params.id);
    if (id !== post.user.id) {
      throw new Error(`update post with id ${req.params.id} is not allowed for user with id ${id}`);
    }
  } catch (err) {
    next(err);
  } finally {
    next();
  }
};
