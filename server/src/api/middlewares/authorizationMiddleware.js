/* eslint-disable no-continue */
import jwtMiddleware from './jwtMiddleware';

const isPathEqual = (route, reqPath) => {
  const routeArr = route.split('/');
  const reqPathArr = reqPath.split('/');
  if (routeArr.length > reqPathArr.length) {
    return false;
  }

  for (let i = 0; i < reqPathArr.length; i++) {
    if (reqPathArr[i] === routeArr[i]) {
      continue;
    }

    if (routeArr[i] === '*') {
      return true;
    }
    return false;
  }
  return true;
};

export default (routesWhiteList = []) => (req, res, next) => (
  routesWhiteList.some(route => isPathEqual(route, req.path))
    ? next()
    : jwtMiddleware(req, res, next) // auth the user if requested path isn't from the white list
);
