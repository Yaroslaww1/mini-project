import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import resetPasswordMiddleware from '../middlewares/resetPasswordMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))

  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))

  .post('/reset-password', (req, res, next) => authService.resetPasswordByEmail(req.body.email)
    .then(data => res.send(data))
    .catch(next))

  .post('/reset-password/:userId/:token',
    resetPasswordMiddleware,
    (req, res, next) => userService.updateUserPasswordById(req.params.userId, req.body.password)
      .then(data => res.send(data))
      .catch(next))

  .put('/user/update', jwtMiddleware, (req, res, next) => userService.updateUserById(req.user.id, req.body.user)
    .then(data => res.send(data))
    .catch(next))

  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next));

export default router;
