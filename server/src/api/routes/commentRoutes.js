import { Router } from 'express';
import * as commentService from '../services/commentService';
import { canEditComment } from '../middlewares/commentMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))

  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))

  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(commentInfo => {
      const { comment, result: reaction } = commentInfo;
      if (reaction && comment && reaction.userId && comment.userId !== reaction.userId) {
        // notify a user if someone (not himself) liked his post

        if (reaction.isLike) {
          req.io.to(comment.userId).emit('like_comment', 'Your comment was liked!');
        } else {
          req.io.to(comment.userId).emit('dislike_comment', 'Your comment was disliked!');
        }
      }
      return res.send(commentInfo.comment);
    })
    .catch(next))

  .put('/:id', canEditComment, (req, res, next) => commentService.updateCommentById(req.params.id, req.body.body)
    .then(comment => {
      req.io.to(comment.userId).emit('update_comment', 'You have updated your comment!');
      return res.send(comment);
    })
    .catch(next))

  .delete('/:id', canEditComment, (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(comment => {
      req.io.to(comment.userId).emit('delete_comment', 'You have deleted your comment!');
      return res.send(comment);
    })
    .catch(next));

export default router;
