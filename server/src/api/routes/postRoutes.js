import { Router } from 'express';
import * as postService from '../services/postService';
import { canEditPost } from '../middlewares/postMiddleware';
import * as emailService from '../services/emailService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))

  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))

  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))

  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(postInfo => {
      const { post, result: reaction } = postInfo;
      if (reaction && post && reaction.userId && post.userId !== reaction.userId) {
        // notify a user if someone (not himself) liked his post

        if (req.body.isLike) {
          req.io.to(post.userId).emit('like_post', 'Your post was liked!');
          emailService.sendNotificationEmail(req.user, post.user, `http://localhost:3000/share/${post.id}`);
        } else {
          req.io.to(post.userId).emit('dislike_post', 'Your post was disliked!');
        }
      }
      return res.send(postInfo.post);
    })
    .catch(next))

  .put('/:id', canEditPost, (req, res, next) => postService.updatePostById(req.params.id, req.body.body)
    .then(post => {
      req.io.to(post.userId).emit('update_post', 'You have updated your post!');
      return res.send(post);
    })
    .catch(next))

  .delete('/:id', canEditPost, (req, res, next) => postService.deletePostById(req.params.id)
    .then(post => {
      req.io.to(post.userId).emit('delete_post', 'You have deleted your post!');
      return res.send(post);
    })
    .catch(next))

  .post('/share', (req, res, next) => emailService.sendSharePostLink(
    req.body.user.username, req.body.receiverEmail, req.body.postLink
  )
    .then(result => res.send(result))
    .catch(next));

export default router;
