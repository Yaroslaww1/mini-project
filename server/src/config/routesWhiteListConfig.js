export default [
  '/auth/login',
  '/auth/register',
  '/auth/reset-password',
  '/auth/reset-password/*'
];
