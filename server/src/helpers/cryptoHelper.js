import bcrypt from 'bcrypt';
import crypto from 'crypto';

const saltRounds = 10;

export const encrypt = data => bcrypt.hash(data, saltRounds);

export const encryptSync = data => bcrypt.hashSync(data, saltRounds);

export const compare = (data, encrypted) => bcrypt.compare(data, encrypted);

export const getRandomStringToken = () => crypto.randomBytes(32).toString('hex');
