export default (orm, DataTypes) => {
  const ResetPassword = orm.define('resetPassword', {
    resetPasswordToken: {
      allowNull: false,
      type: DataTypes.STRING
    },
    expire: {
      allowNull: false,
      type: DataTypes.DATE
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return ResetPassword;
};
