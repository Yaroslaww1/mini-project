import orm from '../db/connection';
import associate from '../db/associations';

const User = orm.import('./user');
const Post = orm.import('./post');
const PostReaction = orm.import('./postReaction');
const Comment = orm.import('./comment');
const CommentReaction = orm.import('./commentReaction');
const Image = orm.import('./image');
const ResetPassword = orm.import('./resetPassword');

associate({
  User,
  Post,
  PostReaction,
  Comment,
  Image,
  CommentReaction,
  ResetPassword
});

export {
  User as UserModel,
  Post as PostModel,
  PostReaction as PostReactionModel,
  Comment as CommentModel,
  Image as ImageModel,
  CommentReaction as CommentReactionModel,
  ResetPassword as ResetPasswordModel
};
