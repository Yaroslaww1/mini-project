import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import sequelize from '../db/connection';

// const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id',
        'commentReactions->user.id',
        'commentReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "commentReactions" as "commentReaction"
                        WHERE "commentReaction"."commentId" = "comment"."id"
                        AND "commentReaction"."isLike" IS TRUE )`), 'likeCount'],
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "commentReactions" as "commentReaction"
                        WHERE "commentReaction"."commentId" = "comment"."id"
                        AND "commentReaction"."isLike" IS FALSE )`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: ['id', 'isLike'],
          // separate: true,
          include: {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }
      ]
    }).then(result => {
      const newResult = result;
      newResult.setDataValue(
        'likeReactions',
        result.commentReactions.filter(commentReaction => commentReaction.isLike === true)
      );
      newResult.setDataValue(
        'dislikeReactions',
        result.commentReactions.filter(commentReaction => commentReaction.isLike === false)
      );
      return newResult;
    });
  }

  updateCommentById(id, body) {
    return Promise.resolve(this.updateById(id, { body }));
  }

  deleteCommentById(id) {
    return this.deleteById(id);
  }
}

export default new CommentRepository(CommentModel);
