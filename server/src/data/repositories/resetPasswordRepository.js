import moment from 'moment';
import { ResetPasswordModel } from '../models/index';
import BaseRepository from './baseRepository';

class ResetPasswordRepository extends BaseRepository {
  addResetPassword(userId, resetPasswordToken) {
    return this.create({
      userId,
      resetPasswordToken,
      expire: moment.utc().add(1000000000, 'seconds')
    });
  }

  getResetPasswordByUserId(userId) {
    return this.model.findOne({
      where: {
        userId
      }
    });
  }

  async createOrUpdateResetPassword(userId, resetPasswordToken) {
    const resetPassword = await this.getResetPasswordByUserId(userId);
    if (resetPassword) {
      return this.updateResetPassword(userId, resetPasswordToken);
    }
    return this.addResetPassword(userId, resetPasswordToken);
  }

  async updateResetPassword(userId, resetPasswordToken) {
    const result = await this.model.update({ resetPasswordToken }, {
      where: { userId },
      returning: true,
      plain: true
    });

    return result[1];
  }
}

export default new ResetPasswordRepository(ResetPasswordModel);
