import { PostReactionModel, PostModel, UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id',
        'user.id'
      ],
      where: { userId, postId },
      include: [
        {
          model: PostModel,
          attributes: ['id', 'userId']
        },
        {
          model: UserModel,
          attributes: ['id', 'username']
        }
      ]
    });
  }

  getPostReactions(postId) {
    return this.model.findAll({
      where: { postId }
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
