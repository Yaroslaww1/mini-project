import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';
import postReactionRepository from './postReactionRepository';

async function myfilter(arr, callback) {
  const fail = Symbol('fail-symbol');
  return (await Promise.all(arr.map(async item => (await callback(item) ? item : fail)))).filter(i => i !== fail);
}

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      showUserId,
      hideUserId,
      likedByUserId
    } = filter;

    const where = {};
    if (showUserId) {
      Object.assign(where, { userId: showUserId });
    }
    if (hideUserId) {
      Object.assign(where, { userId: { [Op.not]: hideUserId } });
    }
    if (likedByUserId) {
      const posts = await this.getPosts({ ...filter, likedByUserId: undefined });
      return myfilter(posts, async post => {
        const reactions = await postReactionRepository.getPostReactions(post.id);
        return reactions.some(reaction => reaction.userId === likedByUserId && reaction.isLike === true);
      });
    }

    return this.model.findAll({
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "postReactions" as "postReaction"
                        WHERE "postReaction"."postId" = "post"."id"
                        AND "postReaction"."isLike" IS TRUE )`), 'likeCount'],
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "postReactions" as "postReaction"
                        WHERE "postReaction"."postId" = "post"."id"
                        AND "postReaction"."isLike" IS FALSE )`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: PostReactionModel,
          separate: true,
          include: {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }
      ],
      order: [
        ['createdAt', 'DESC'],
        ['id', 'DESC']
      ],
      offset: parseInt(offset, 10),
      limit: parseInt(limit, 10)
    }).then(result => {
      const newResult = result;
      newResult.forEach((post, index) => {
        newResult[index].setDataValue(
          'likeReactions',
          post.postReactions.filter(postReaction => postReaction.isLike === true)
        );
        newResult[index].setDataValue(
          'dislikeReactions',
          post.postReactions.filter(postReaction => postReaction.isLike === false)
        );
      });
      return newResult;
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id',
        'comments->commentReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "postReactions" as "postReaction"
                        WHERE "postReaction"."postId" = "post"."id"
                        AND "postReaction"."isLike" IS TRUE )`), 'likeCount'],
          [sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM "postReactions" as "postReaction"
                        WHERE "postReaction"."postId" = "post"."id"
                        AND "postReaction"."isLike" IS FALSE )`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: CommentModel,
          attributes: {
            include: [
              [sequelize.literal(`(
                SELECT COUNT(*)
                FROM "commentReactions" as "commentReaction"
                WHERE "commentReaction"."commentId" = "comments"."id"
                AND "commentReaction"."isLike" IS TRUE )`), 'likeCount'],
              [sequelize.literal(`(
                SELECT COUNT(*)
                FROM "commentReactions" as "commentReaction"
                WHERE "commentReaction"."commentId" = "comments"."id"
                AND "commentReaction"."isLike" IS FALSE )`), 'dislikeCount']
            ]
          },
          include: [
            {
              model: UserModel,
              attributes: ['id', 'username', 'status', 'email'],
              include: [
                {
                  model: ImageModel,
                  attributes: ['id', 'link']
                }
              ]
            },
            {
              model: CommentReactionModel,
              attributes: ['id', 'isLike'],
              // separate: true,
              include: {
                model: UserModel,
                attributes: ['id', 'username', 'status'],
                include: {
                  model: ImageModel,
                  attributes: ['id', 'link']
                }
              }
            }
          ]
        },
        {
          model: UserModel,
          attributes: ['id', 'username', 'status', 'email'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: PostReactionModel,
          separate: true,
          include: {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }
      ]
    }).then(result => {
      const newResult = result;
      newResult.setDataValue(
        'likeReactions',
        result.postReactions.filter(postReaction => postReaction.isLike === true)
      );
      newResult.setDataValue(
        'dislikeReactions',
        result.postReactions.filter(postReaction => postReaction.isLike === false)
      );
      newResult.comments.forEach((comment, index) => {
        newResult.comments[index].setDataValue(
          'likeReactions',
          comment.commentReactions.filter(commentReaction => commentReaction.isLike === true)
        );
        newResult.comments[index].setDataValue(
          'dislikeReactions',
          comment.commentReactions.filter(commentReaction => commentReaction.isLike === false)
        );
      });
      return newResult;
    });
  }

  updatePostById(id, body) {
    return Promise.resolve(this.updateById(id, { body }));
  }

  deletePostById(id) {
    return this.deleteById(id);
  }
}

export default new PostRepository(PostModel);
