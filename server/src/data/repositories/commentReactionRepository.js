import { CommentReactionModel, CommentModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(commentId) {
    return this.model.findAll({
      where: { commentId }
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
