export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('resetPasswords', 'createdAt', {
        type: Sequelize.DATE,
        allowNull: true,
        validate: {}
      }, { transaction }),
      queryInterface.addColumn('resetPasswords', 'updatedAt', {
        type: Sequelize.DATE,
        allowNull: true,
        validate: {}
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('resetPasswords', 'createdAt', { transaction }),
      queryInterface.removeColumn('resetPasswords', 'updatedAt', { transaction })
    ]))
};
