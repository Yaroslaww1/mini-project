export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('posts', 'deletedAt', {
        type: Sequelize.DATE,
        allowNull: true,
        validate: {}
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('posts', 'deletedAt', { transaction })
    ]))
};
