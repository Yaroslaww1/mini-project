export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'status', {
        type: Sequelize.TEXT,
        allowNull: true,
        validate: {},
        defaultValue: 'No status set'
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'status', { transaction })
    ]))
};
