import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import LikeButton from './likeButton';
import DislikeButton from './dislikeButton';

// import styles from './styles.module.scss';

const Comment = ({ comment, likeComment, dislikeComment }) => {
  const {
    id,
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount,
    likeReactions,
    dislikeReactions
  } = comment;

  return (
    <CommentUI>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          <div style={{ opacity: '75%' }}>{user.status}</div>
          {' '}
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action>
            <LikeButton
              likeComment={() => likeComment(id)}
              likeCount={likeCount}
              likeReactions={likeReactions}
            />
          </CommentUI.Action>
          <CommentUI.Action>
            <DislikeButton
              dislikeComment={() => dislikeComment(id)}
              dislikeCount={dislikeCount}
              dislikeReactions={dislikeReactions}
            />
          </CommentUI.Action>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
