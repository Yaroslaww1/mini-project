import React from 'react';
import PropTypes, { any } from 'prop-types';
import { Label, Icon, Popup, Grid, Header, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

function dislikeButton({ dislikeComment, dislikeCount, dislikeReactions }) {
  return (
    <Popup
      trigger={(
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={dislikeComment}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      )}
      flowing
      hoverable
    >
      <Grid centered divided columns={dislikeReactions.length}>
        {
          dislikeReactions.map(likeReaction => (
            <Grid.Column textAlign="center">
              <Header as="h6">{likeReaction.user.username}</Header>
              <Image src={getUserImgLink(likeReaction.user.image)} className="avatar" />
            </Grid.Column>
          ))
        }
      </Grid>
    </Popup>
  );
}

dislikeButton.propTypes = {
  dislikeComment: PropTypes.func.isRequired,
  dislikeCount: PropTypes.number.isRequired,
  dislikeReactions: PropTypes.arrayOf(PropTypes.objectOf(any)).isRequired
};

export default dislikeButton;

