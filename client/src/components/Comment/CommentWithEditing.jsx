import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Input, Button, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const CommentWithEditing = ({ comment, editComment, deleteComment, likeComment, dislikeComment }) => {
  const {
    body: initialBody,
    createdAt,
    user,
    id,
    likeCount,
    dislikeCount
  } = comment;

  const [body, setBody] = useState(initialBody);
  const [editing, setEditing] = useState(false);

  const handleBodyChange = event => {
    event.preventDefault();
    setBody(event.target.value);
  };

  const saveComment = async () => {
    await editComment({ commentId: id, body });
    setEditing(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          <div style={{ opacity: '75%' }}>{user.status}</div>
          {' '}
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {editing
            ? (
              <>
                <Input
                  placeholder={body}
                  onChange={handleBodyChange}
                  value={body}
                />
                <Button onClick={saveComment}>
                  Save
                </Button>
              </>
            )
            : body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          </CommentUI.Action>
          <CommentUI.Action>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          </CommentUI.Action>
          <CommentUI.Action>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setEditing(true)}>
              Edit
            </Label>
          </CommentUI.Action>
          <CommentUI.Action>
            <Label basic size="small" as="a" className={styles.toolbarBtnDanger} onClick={() => deleteComment(comment)}>
              Delete
            </Label>
          </CommentUI.Action>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

CommentWithEditing.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default CommentWithEditing;
