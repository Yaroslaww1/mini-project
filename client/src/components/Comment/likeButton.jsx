import React from 'react';
import PropTypes, { any } from 'prop-types';
import { Label, Icon, Popup, Grid, Header, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

function likeButton({ likeComment, likeCount, likeReactions }) {
  return (
    <Popup
      trigger={(
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={likeComment}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
      )}
      flowing
      hoverable
    >
      <Grid centered divided columns={likeReactions.length}>
        {
          likeReactions.map(likeReaction => (
            <Grid.Column textAlign="center">
              <Header as="h6">{likeReaction.user.username}</Header>
              <Image src={getUserImgLink(likeReaction.user.image)} className="avatar" />
            </Grid.Column>
          ))
        }
      </Grid>
    </Popup>
  );
}

likeButton.propTypes = {
  likeComment: PropTypes.func.isRequired,
  likeCount: PropTypes.number.isRequired,
  likeReactions: PropTypes.arrayOf(PropTypes.objectOf(any)).isRequired
};

export default likeButton;

