import React from 'react';
import PropTypes, { any } from 'prop-types';
import { Label, Icon, Popup, Grid, Header, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

function dislikeButton({ dislikePost, dislikeCount, dislikeReactions }) {
  return (
    <Popup
      trigger={(
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={dislikePost}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      )}
      flowing
      hoverable
    >
      <Grid centered divided columns={dislikeReactions.length}>
        {
          dislikeReactions.map(dislikeReaction => (
            <Grid.Column textAlign="center">
              <Header as="h6">{dislikeReaction.user.username}</Header>
              <Image src={getUserImgLink(dislikeReaction.user.image)} className="avatar" />
            </Grid.Column>
          ))
        }
      </Grid>
    </Popup>
  );
}

dislikeButton.propTypes = {
  dislikePost: PropTypes.func.isRequired,
  dislikeCount: PropTypes.number.isRequired,
  dislikeReactions: PropTypes.arrayOf(PropTypes.objectOf(any)).isRequired
};

export default dislikeButton;

