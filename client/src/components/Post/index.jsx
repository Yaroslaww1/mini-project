import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import LikeButton from './likeButton';
import DislikeButton from './dislikeButton';

import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost }) => {
  const {
    id,
    image,
    user,
    body,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likeReactions,
    dislikeReactions
  } = post;
  const date = moment(createdAt).fromNow();

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <LikeButton
          likePost={() => likePost(id)}
          likeCount={likeCount}
          likeReactions={likeReactions}
        />
        <DislikeButton
          dislikePost={() => dislikePost(id)}
          dislikeCount={dislikeCount}
          dislikeReactions={dislikeReactions}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
