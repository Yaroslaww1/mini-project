import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Input, Button } from 'semantic-ui-react';
import moment from 'moment';

import LikeButton from './likeButton';
import DislikeButton from './dislikeButton';

import styles from './styles.module.scss';

const PostWithEditing = ({ post, likePost, dislikePost, editPost, deletePost, toggleExpandedPost, sharePost }) => {
  const {
    id,
    image,
    body: initialBody,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likeReactions,
    dislikeReactions
  } = post;
  const date = moment(createdAt).fromNow();

  const [body, setBody] = useState(initialBody);
  const [editing, setEditing] = useState(false);

  const handleBodyChange = event => {
    event.preventDefault();
    setBody(event.target.value);
  };

  const savePost = async () => {
    await editPost({ postId: id, body });
    setEditing(false);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {editing
            ? (
              <>
                <Input
                  placeholder={body}
                  onChange={handleBodyChange}
                  value={body}
                />
                <Button onClick={savePost}>
                  Save
                </Button>
              </>
            )
            : body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <LikeButton
          likePost={() => likePost(id)}
          likeCount={likeCount}
          likeReactions={likeReactions}
        />
        <DislikeButton
          dislikePost={() => dislikePost(id)}
          dislikeCount={dislikeCount}
          dislikeReactions={dislikeReactions}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setEditing(true)}>
          Edit post
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtnDanger} onClick={() => deletePost(post)}>
          Delete post
        </Label>
      </Card.Content>
    </Card>
  );
};

PostWithEditing.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default PostWithEditing;
