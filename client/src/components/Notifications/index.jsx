import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost }) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);

    socket.on('like_post', () => {
      NotificationManager.info('Your post was liked!');
    });
    socket.on('like_comment', () => {
      NotificationManager.info('Your comment was liked!');
    });
    socket.on('dislike_post', () => {
      NotificationManager.info('Your post was disliked!');
    });
    socket.on('dislike_comment', () => {
      NotificationManager.info('Your comment was disliked!');
    });

    socket.on('new_post', post => {
      if (post.userId !== id) {
        applyPost(post.id);
        NotificationManager.info('New post!');
      }
    });

    socket.on('update_post', () => {
      NotificationManager.info('You have successfully updated your post!');
    });
    socket.on('update_comment', () => {
      NotificationManager.info('You have successfully updated your comment!');
    });

    socket.on('delete_post', () => {
      NotificationManager.info('You have successfully deleted your post!');
    });
    socket.on('delete_comment', () => {
      NotificationManager.info('You have successfully deleted your comment!');
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired
};

export default Notifications;
