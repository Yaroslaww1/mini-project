/* eslint-disable no-unused-vars */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close, sharePost: propsSharePost }) => {
  const [doneText, setDoneText] = useState(false);
  const [sendEmail, setSendEmail] = useState(false);
  const [email, setEmail] = useState(false);
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setDoneText('Copied');
  };

  const sharePost = async postLink => {
    try {
      await propsSharePost(email, postLink);
      setDoneText('Send');
    } catch (err) {
      setDoneText(`Error occured: ${err.message || 'Unhandled'}`);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {doneText && (
          <span>
            <Icon color="green" name="copy" />
            {doneText}
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => { input = ref; }}
        />
        {!sendEmail
          ? (
            <Button
              onClick={() => setSendEmail(true)}
            >
              Share by email
            </Button>
          )
          : (
            <>
              <Input
                fluid
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                onChange={ev => setEmail(ev.target.value)}
              />
              <Button
                onClick={() => sharePost(`${window.location.origin}/share/${postId}`)}
              >
                Share
              </Button>
            </>
          )}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default SharedPostLink;
