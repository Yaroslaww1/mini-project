import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Input } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, saveStatus }) => {
  const [editing, setEditing] = useState(false);
  const [status, setStatus] = useState(user.status);

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI>
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={4}>
                      <Image circular src={getUserImgLink(user.image)} />
                    </Grid.Column>
                    <Grid.Column width={9}>
                      {' '}
                      {user.username}
                      <Grid>
                        <Grid.Row>
                          <Grid.Column width={10}>
                            <Input
                              size="mini"
                              value={status}
                              disabled={!editing}
                              onChange={e => setStatus(e.target.value)}
                            />
                          </Grid.Column>
                          <Grid.Column width={2}>
                            {!editing
                              ? (
                                <Button onClick={() => setEditing(true)}>
                                  <Icon name="pencil alternate" />
                                </Button>
                              )
                              : (
                                <Button onClick={() => { saveStatus(status); setEditing(false); }}>
                                  Save
                                </Button>
                              )}
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  saveStatus: PropTypes.func.isRequired
};

export default Header;
