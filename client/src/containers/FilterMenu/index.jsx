/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Menu, Icon, Button, Checkbox } from 'semantic-ui-react';
import {
  setFilter,
  loadPosts
} from '../Thread/actions';

function Filters({
  filter,
  userId,
  setFilter: _setFilter,
  loadPosts: _loadPosts
}) {
  const [open, setOpen] = useState(false);

  const toggleOpen = () => {
    setOpen(prevState => !prevState);
  };

  const toggleFilterProperty = newProperty => {
    const key = Object.keys(newProperty)[0];
    if (filter[key]) {
      _setFilter({ ...filter, from: 0, [key]: undefined });
    } else {
      _setFilter({ ...filter, from: 0, ...newProperty });
    }
  };

  const applyFilters = () => {
    /*
      Fix problem with multiply applying one filter
      ("from" parametr increase after clicking on button "apply filter" and some posts dissapear)
    */
    _loadPosts({ ...filter, from: 0 });
    toggleOpen();
  };

  return (
    <>
      <Menu text>
        <Menu.Item header>
          <Button icon onClick={() => toggleOpen()}>
            <Icon name="filter" />
            Filter
          </Button>
        </Menu.Item>
      </Menu>
      {open && (
        <>
          <div className="column">
            <Checkbox
              toggle
              className="ui top attached segment"
              label="Show only my posts"
              disabled={filter.hideUserId === userId}
              checked={filter.showUserId === userId}
              onChange={() => toggleFilterProperty({ showUserId: userId })}
            />
            <Checkbox
              toggle
              className="ui attached segment"
              label="Hide my posts"
              disabled={filter.showUserId === userId}
              checked={filter.hideUserId === userId}
              onChange={() => toggleFilterProperty({ hideUserId: userId })}
            />
            <Checkbox
              toggle
              className="ui attached segment"
              label="Show posts liked by me only"
              checked={filter.likedByUserId === userId}
              onChange={() => toggleFilterProperty({ likedByUserId: userId })}
            />
            <Button
              onClick={applyFilters}
            >
              Apply filters
            </Button>
          </div>
        </>
      )}
    </>
  );
}

Filters.propTypes = {
  filter: PropTypes.objectOf(PropTypes.any).isRequired,
  setFilter: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  loadPosts: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  userId: rootState.profile.user.id,
  filter: rootState.posts.filter
});

const actions = {
  setFilter,
  loadPosts
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);

