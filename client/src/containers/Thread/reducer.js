import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  UPDATE_POST,
  DELETE_POST,
  UPDATE_COMMENT,
  DELETE_COMMENT,
  SET_FILTER
} from './actionTypes';

export default (state = {}, action) => {
  console.log(state, action);
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST: {
      const newPosts = state.posts.map(post => (
        post.id === action.post.id
          ? { ...post, body: action.post.body }
          : post
      ));
      return {
        ...state,
        posts: newPosts
      };
    }
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post.id !== action.post.id)
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case UPDATE_COMMENT: {
      const newComments = state.expandedPost.comments.map(comment => (
        comment.id === action.comment.id
          ? { ...comment, body: action.comment.body }
          : comment
      ));
      return {
        ...state,
        expandedPost: {
          ...state.expandedPost,
          comments: newComments
        }
      };
    }
    case DELETE_COMMENT: {
      return {
        ...state,
        expandedPost: {
          ...state.expandedPost,
          comments: state.expandedPost.comments.filter(comment => comment.id !== action.comment.id)
        }
      };
    }
    case SET_FILTER: {
      return {
        ...state,
        filter: action.filter
      };
    }
    default:
      return state;
  }
};
