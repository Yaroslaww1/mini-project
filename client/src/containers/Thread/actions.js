import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  DELETE_POST,
  UPDATE_COMMENT,
  DELETE_COMMENT,
  SET_FILTER
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const updateCommentAction = comment => ({
  type: UPDATE_COMMENT,
  comment
});

const deletePostAction = post => ({
  type: DELETE_POST,
  post
});

const deleteCommentAction = comment => ({
  type: DELETE_COMMENT,
  comment
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setFilterAction = filter => ({
  type: SET_FILTER,
  filter
});

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

// ACTIONS WITH POSTS
export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const editPost = ({ postId, body }) => async dispatch => {
  const { id } = await postService.editPost(postId, body);
  const newPost = await postService.getPost(id);
  dispatch(updatePostAction(newPost));
};

export const deletePost = post => async dispatch => {
  await postService.deletePost(post);
  dispatch(deletePostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount, likeReactions, dislikeReactions } = await postService.likePost(postId);

  const mapLikes = post => ({
    ...post,
    likeReactions,
    dislikeReactions,
    likeCount,
    dislikeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount, likeReactions, dislikeReactions } = await postService.dislikePost(postId);

  const mapDislikes = post => ({
    ...post,
    likeReactions,
    dislikeReactions,
    likeCount,
    dislikeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

// ACTIONS WITH COMMENTS
export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = ({ commentId, body }) => async dispatch => {
  const { id } = await commentService.editComment(commentId, body);
  const newComment = await commentService.getComment(id);
  dispatch(updateCommentAction(newComment));
};

export const deleteComment = comment => async dispatch => {
  await commentService.deleteComment(comment);
  dispatch(deleteCommentAction(comment));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount, likeReactions, dislikeReactions } = await commentService.likeComment(commentId);

  const mapLikes = comment => ({
    ...comment,
    likeReactions,
    dislikeReactions,
    likeCount,
    dislikeCount
  });

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const updated = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setExpandedPostAction({
    ...expandedPost,
    comments: updated
  }));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount, likeReactions, dislikeReactions } = await commentService.dislikeComment(commentId);

  const mapDislikes = comment => ({
    ...comment,
    likeReactions,
    dislikeReactions,
    likeCount,
    dislikeCount
  });

  const { posts: { expandedPost } } = getRootState();
  const { comments } = expandedPost;
  const updated = comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

  dispatch(setExpandedPostAction({
    ...expandedPost,
    comments: updated
  }));
};

// ACTIONS WITH FILTER
export const setFilter = filter => async dispatch => {
  dispatch(setFilterAction(filter));
};
