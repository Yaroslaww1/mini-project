/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post/index';
import PostWithEditing from 'src/components/Post/PostWithEditing';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import FilterMenu from 'src/containers/FilterMenu';
import {
  loadPosts,
  loadMorePosts,
  likePost, dislikePost,
  toggleExpandedPost,
  addPost,
  editPost,
  deletePost,
  setFilter
} from './actions';

import { sharePostByEmail } from '../../services/postService';

import styles from './styles.module.scss';

const Thread = ({
  userId,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  editPost: modifyPost,
  likePost: like,
  dislikePost: dislike,
  deletePost: removePost,
  toggleExpandedPost: toggle,
  filter: postsFilter,
  setFilter: _setFilter,
  user
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    const newFilter = postsFilter;
    newFilter.from = from + count;
    _setFilter(newFilter);
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <FilterMenu />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.length > 0
          ? posts.map(post => (
            post.user.id !== userId
              ? (
                <Post
                  post={post}
                  likePost={like}
                  dislikePost={dislike}
                  toggleExpandedPost={toggle}
                  sharePost={sharePost}
                  key={post.id}
                />
              )
              : (
                <PostWithEditing
                  post={post}
                  likePost={like}
                  dislikePost={dislike}
                  editPost={modifyPost}
                  deletePost={removePost}
                  toggleExpandedPost={toggle}
                  sharePost={sharePost}
                  key={post.id}
                />
              )
          ))
          : <p>No posts found :(</p>}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
        />
      )}
      {sharedPostId
        && (
          <SharedPostLink
            postId={sharedPostId}
            close={() => setSharedPostId(undefined)}
            sharePost={(email, postLink) => sharePostByEmail(user, email, postLink)}
          />
        )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  filter: PropTypes.objectOf(PropTypes.any),
  setFilter: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined,
  filter: {
    showUserId: undefined,
    hideUserId: undefined,
    from: 0,
    count: 10,
    likedByUserId: undefined
  }
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id,
  user: rootState.profile.user,
  filter: rootState.posts.filter
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  editPost,
  deletePost,
  setFilter
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
