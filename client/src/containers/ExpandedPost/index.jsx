import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  toggleExpandedPost,
  addComment,
  editComment,
  deleteComment,
  likeComment,
  dislikeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment/index';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import CommentWithEditing from 'src/components/Comment/CommentWithEditing';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  toggleExpandedPost: toggle,
  addComment: add,
  editComment: _editComment,
  likeComment: _likeComment,
  dislikeComment: _dislikeComment,
  userId,
  deleteComment: removeComment
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                comment.user.id !== userId
                  ? (
                    <Comment
                      key={comment.id}
                      comment={comment}
                      editComment={_editComment}
                      likeComment={_likeComment}
                      dislikeComment={_dislikeComment}
                    />
                  )
                  : (
                    <CommentWithEditing
                      key={comment.id}
                      comment={comment}
                      editComment={_editComment}
                      deleteComment={removeComment}
                      likeComment={_likeComment}
                      dislikeComment={_dislikeComment}
                    />
                  )
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = { likePost, toggleExpandedPost, addComment, editComment, deleteComment, likeComment, dislikeComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
