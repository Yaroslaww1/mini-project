/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button,
  Segment,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import {
  updateUser
} from './actions';

const uploadImage = file => imageService.uploadImage(file);

const Profile = ({ user: propsUser, updateUser: propsUpdateUser }) => {
  const [editing, setEditing] = useState(false);
  const [user, setUser] = useState(propsUser);
  const [isUploading, setIsUploading] = useState(false);

  const editProperty = event => {
    event.preventDefault();
    setUser({
      ...user,
      [event.target.name]: event.target.value
    });
  };

  const saveChanges = async () => {
    await propsUpdateUser(user);
    setEditing(false);
  };

  const editProfileImage = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      const newUser = {
        ...user,
        image: {
          id: imageId,
          link: imageLink
        },
        imageId
      };
      await propsUpdateUser(newUser);
      setUser(newUser);
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <>
      {isUploading === true ? (
        <Segment>
          <Dimmer active inverted>
            <Loader inverted size="massive">Loading</Loader>
          </Dimmer>
        </Segment>
      )
        : (
          <Grid container textAlign="center" style={{ paddingTop: 30 }}>
            <Grid.Column>
              <Button for="file" as="label" hidden>
                <Image
                  centered
                  src={getUserImgLink(user.image)}
                  size="medium"
                  circular
                />
                <input name="image" id="file" type="file" onChange={editProfileImage} hidden />
              </Button>
              <br />
              <Input
                icon="user"
                iconPosition="left"
                placeholder="Username"
                type="text"
                name="username"
                disabled={!editing}
                value={user.username}
                onChange={editProperty}
              />
              <br />
              <br />
              <Input
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                name="email"
                disabled
                value={user.email}
                onChange={editProperty}
              />
              <br />
              <br />
              <Input
                icon="star"
                iconPosition="left"
                placeholder="Status"
                type="text"
                name="status"
                disabled={!editing}
                value={user.status}
                onChange={editProperty}
              />
              <br />
              <br />
              <Button
                onClick={() => setEditing(true)}
              >
                Edit
              </Button>
              <br />
              <br />
              {editing && (
                <Button
                  color="green"
                  onClick={saveChanges}
                >
                  Save
                </Button>
              )}
            </Grid.Column>
          </Grid>
        )}
    </>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUser: PropTypes.func
};

Profile.defaultProps = {
  user: {},
  updateUser: () => {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
