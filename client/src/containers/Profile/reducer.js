import { SET_USER, SET_ERROR } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_ERROR:
      return {
        ...state,
        errors: {
          [action.errorType]: action.error
        }
      };
    default:
      return state;
  }
};
