import * as authService from 'src/services/authService';
import { SET_USER, SET_ERROR } from './actionTypes';
import { RESET_PASSWORD, CONFIRM_RESET_PASSWORD } from './actionErrorTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setError = (error, errorType) => async dispatch => dispatch({
  type: SET_ERROR,
  error,
  errorType
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateUser = user => async (dispatch, getRootState) => {
  const newUser = await authService.updateUser({ user });
  setUser(newUser)(dispatch, getRootState);
};

export const resetPasswordByEmail = email => async (dispatch, getRootState) => {
  try {
    const result = await authService.resetPasswordByEmail({ email });
    setError(result, RESET_PASSWORD)(dispatch, getRootState);
  } catch (err) {
    setError({ error: 'Unhandled error! Please try again later' }, RESET_PASSWORD)(dispatch, getRootState);
  }
};

export const confirmResetPassword = (password, userId, token) => async (dispatch, getRootState) => {
  try {
    const result = await authService.confirmResetPassword({ password }, userId, token);
    setError(result, CONFIRM_RESET_PASSWORD)(dispatch, getRootState);
  } catch (err) {
    setError({ error: 'Unhandled error! Please try again later' }, CONFIRM_RESET_PASSWORD)(dispatch, getRootState);
  }
};
