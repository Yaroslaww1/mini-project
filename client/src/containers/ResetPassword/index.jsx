/* eslint-disable no-unused-vars */
/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Button, Segment, Grid } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  resetPasswordByEmail
} from 'src/containers/Profile/actions';

const ResetPassword = ({ resetPasswordByEmail: resetPassword, error }) => {
  const [send, setSend] = useState(false);
  const [email, setEmail] = useState('');
  // const [text, setText] = useState();

  const emailChanged = data => {
    setEmail(data);
  };

  const handleResetClick = async () => {
    await resetPassword(email);
    setSend(true);
  };

  return (
    <>
      {send === false
        ? (
          <Grid textAlign="center" verticalAlign="middle" className="fill">
            <Grid.Column style={{ maxWidth: 450 }}>
              <Form name="loginForm" size="large" onSubmit={handleResetClick}>
                <Segment>
                  <h3>Enter email address of existing account. We will send a letter with reset link to this email</h3>
                  <Form.Input
                    fluid
                    icon="at"
                    iconPosition="left"
                    placeholder="Email"
                    type="email"
                    onChange={ev => emailChanged(ev.target.value)}
                  />
                  <Button
                    type="submit"
                    color="teal"
                    fluid
                    size="large"
                    primary
                  >
                    Reset password
                  </Button>
                </Segment>
              </Form>
            </Grid.Column>
          </Grid>
        )
        : (
          <Grid textAlign="center" verticalAlign="middle" className="fill">
            <Grid.Column style={{ maxWidth: 450 }}>
              <h1>{error.error ? error.error : 'Reset password link has been sent. Please check yout email!'}</h1>
              <NavLink to="/login">
                <h1>Back to login</h1>
              </NavLink>
            </Grid.Column>
          </Grid>
        )}
    </>
  );
};

ResetPassword.propTypes = {
  resetPasswordByEmail: PropTypes.func.isRequired,
  error: PropTypes.any
};

ResetPassword.defaultProps = {
  error: undefined
};

const mapStateToProps = rootState => ({
  error: rootState.profile.errors?.RESET_PASSWORD
});

const actions = { resetPasswordByEmail };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword);
