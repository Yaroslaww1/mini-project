/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Form, Button, Segment, Grid } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  confirmResetPassword
} from 'src/containers/Profile/actions';

const ResetPasswordCallback = ({ confirmResetPassword: resetPassword, match, error }) => {
  const [password, setPassword] = useState('');
  const [send, setSend] = useState(false);

  const passwordChanged = data => {
    setPassword(data);
  };

  const handleResetClick = async () => {
    await resetPassword(password, match.params.userId, match.params.token);
    setSend(true);
  };

  return (
    <>
      {send === false
        ? (
          <Grid textAlign="center" verticalAlign="middle" className="fill">
            <Grid.Column style={{ maxWidth: 450 }}>
              <Form name="loginForm" size="large" onSubmit={handleResetClick}>
                <Segment>
                  <h3>Enter new password</h3>
                  <Form.Input
                    fluid
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                    onChange={ev => passwordChanged(ev.target.value)}
                  />
                  <Button
                    type="submit"
                    color="teal"
                    fluid
                    size="large"
                    primary
                  >
                    Reset password
                  </Button>
                </Segment>
              </Form>
            </Grid.Column>
          </Grid>
        )
        : (
          <Grid textAlign="center" verticalAlign="middle" className="fill">
            <Grid.Column style={{ maxWidth: 450 }}>
              <h1>{error.error ? error.error : 'Password has been reset'}</h1>
              <NavLink to="/login">
                <h1>Back to login</h1>
              </NavLink>
            </Grid.Column>
          </Grid>
        )}
    </>
  );
};

ResetPasswordCallback.propTypes = {
  confirmResetPassword: PropTypes.func.isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  error: PropTypes.any
};

ResetPasswordCallback.defaultProps = {
  error: undefined
};

const mapStateToProps = rootState => ({
  error: rootState.profile.errors?.CONFIRM_RESET_PASSWORD
});

const actions = { confirmResetPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPasswordCallback);
