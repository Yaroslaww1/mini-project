import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/user/update',
    type: 'PUT',
    request
  });
  return response.json();
};

export const resetPasswordByEmail = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset-password',
    type: 'POST',
    request
  });
  return response.json();
};

export const confirmResetPassword = async (request, userId, token) => {
  const response = await callWebApi({
    endpoint: `/api/auth/reset-password/${userId}/${token}`,
    type: 'POST',
    request
  });
  return response.json();
};
